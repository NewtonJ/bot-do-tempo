## Generated Story -9092270849361349484
* greet
    - utter_ask_name
* say_name{"name": "newton"}
    - slot{"name": "newton"}
    - action_name
    - slot{"name": "newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}
* inform{"location": "curitiba"}
    - slot{"location": "curitiba"}
    - action_weather
    - slot{"location": "curitiba"}