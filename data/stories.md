## story 01
* greet
    - utter_ask_name

## story 02
* goodbye
    - utter_goodbye

## story 03
* inform
    - utter_ask_location

## story 04
* inform
    - action_weather

## story 05
* say_name
    - action_name

## Generated Story -9092270849361349484
* greet
    - utter_ask_name
* say_name{"name": "newton"}
    - slot{"name": "newton"}
    - action_name
    - slot{"name": "newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}
* inform{"location": "curitiba"}
    - slot{"location": "curitiba"}
    - action_weather
    - slot{"location": "curitiba"}

## Generated Story -8913619677091384240
* greet
    - utter_ask_name
* say_name{"name": "newton"}
    - slot{"name": "newton"}
    - action_name
    - slot{"name": "newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}

## Generated Story -2050967069161032783
* greet
    - utter_ask_name
* say_name{"name": "Newton"}
    - slot{"name": "Newton"}
    - action_name
    - slot{"name": "Newton"}
* inform{"location": "recife"}
    - slot{"location": "recife"}
    - action_weather
    - slot{"location": "recife"}

## Generated Story 7666866953137976675
* greet
    - utter_ask_name
* say_name{"name": "Newton"}
    - slot{"name": "Newton"}
    - action_name
    - slot{"name": "Newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}

## Generated Story -5517016064927831136
* greet
    - utter_ask_name
* say_name{"name": "newton"}
    - slot{"name": "newton"}
    - action_name
    - slot{"name": "newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}

## Generated Story -8194094870408940977
* greet
    - utter_ask_name
* say_name{"name": "newton"}
    - slot{"name": "newton"}
    - action_name
    - slot{"name": "newton"}
* inform{"location": "fortaleza"}
    - slot{"location": "fortaleza"}
    - action_weather
    - slot{"location": "fortaleza"}

