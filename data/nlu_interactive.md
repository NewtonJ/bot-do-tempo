## intent:greet
- Oi

## intent:inform
- Me informa o tempo em [Fortaleza](location:fortaleza)

## intent:say_name
- Meu nome é [Newton](name:newton)
